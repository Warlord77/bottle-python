import re, json
import xml.etree.ElementTree as ET
import os.path
from os import path
import bottle
from bottle import *


app = Bottle()

class EnableCors(object):
    name = 'enable_cors'
    api = 2

    def apply(self, fn, context):
        def _enable_cors(*args, **kwargs):
            # set CORS headers
            response.headers['Access-Control-Allow-Origin'] = '*'
            response.headers['Access-Control-Allow-Methods'] = 'GET, POST, PUT, OPTIONS'
            response.headers['Access-Control-Allow-Headers'] = '*'
            response.headers['Access-Control-Request-Headers'] = '*'
            response.headers['Cache-Control'] = 'no-cache'

            if bottle.request.method != 'OPTIONS':
                # actual request; reply with the actual response
                return fn(*args, **kwargs)

        return _enable_cors

@app.route('/cors', method=['OPTIONS', 'GET'])
def lvambience():
    response.headers['Content-type'] = 'application/json'
    return '[1]'

@app.route('/')
def hello():
    return "Hello World!"

@app.route('/savejourneys', method=['post'])
def add_journeys():
    if request.POST.get('journey'):
        root = ET.fromstring(request.POST.get('journey'))
        arr_rootchild = root.getchildren()
        try:
            filename = arr_rootchild[0].attrib
            filename =str(filename.get('name')).replace(" ", "")
            f= open("journeys/"+filename+".xml","w+")
            f.write(request.POST.get('journey'))
            f.close()
        except Exception as e:
            print("Error while storing "+str(e))


@app.route('/journeys', method=['OPTIONS','GET'])
def journeys():
    response.headers['Content-type'] = 'application/xml'
    if request.GET.get('journey'):
        journeyname = request.GET.get('journey').replace(" ","")
        if path.isfile("journeys/"+str(journeyname)+".xml"):

            f= open("journeys/"+str(journeyname)+".xml","r+")
            print ('OUTPUT IS CORRECT')
            return f.read()
        else:
            print ("HERE IT WILL PRINT THIS")
            return '<?xml version="1.0"?><message>Journey Not Found</message>'

app.install(EnableCors())

run(app, host='localhost', port=8082, debug=True)
